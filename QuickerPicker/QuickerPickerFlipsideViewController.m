//
//  QuickerPickerFlipsideViewController.m
//  QuickerPicker
//
//  Created by Dave Kalin on 4/22/14.
//  Copyright (c) 2014 DK Digital Media. All rights reserved.
//

#import "QuickerPickerFlipsideViewController.h"

@interface QuickerPickerFlipsideViewController ()

@end

@implementation QuickerPickerFlipsideViewController


NSTimer *timer;
UIAlertView *pleaseWait;
bool restorePurchase = FALSE;
int whichProduct = 1;


- (void)awakeFromNib
{
    self.preferredContentSize = CGSizeMake(320.0, 480.0);
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

    //The IAP "Please Wait" alert has no buttons so I'm allocating it now.
    pleaseWait = [[UIAlertView alloc] initWithTitle:@"Tip Jar"
                                            message:@"Please wait..."
                                           delegate:self
                                  cancelButtonTitle:nil
                                  otherButtonTitles:nil];
    
    
    // Check status of IAP
    bool areAdsRemoved = [[NSUserDefaults standardUserDefaults] boolForKey:@"areAdsRemoved"];
    if (areAdsRemoved) {
        _thanksGraphic.hidden = false;
        
    }
    
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(randomColor:) userInfo:nil repeats:YES] ;
    
    // fix the TipJar buttons
    for (UIView * view in [self.view subviews])
    {
        if ([view isKindOfClass:[UIButton class]])
        {
            UIButton * button = (UIButton *)view;
            if (([button.titleLabel.text rangeOfString:@"Tip"].location != NSNotFound))
            {
                [[button layer] setCornerRadius:8.0f];
                [[button layer] setMasksToBounds:YES];
                [[button layer] setBorderWidth:1.0f];
                button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
                button.titleLabel.numberOfLines = 3;
                button.titleLabel.textAlignment = NSTextAlignmentCenter;
                
            }
        }
    }

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)done:(id)sender
{
    [timer invalidate];
    timer = nil;

    [self.delegate flipsideViewControllerDidFinish:self];
}




- (IBAction)randomColor:(id)sender
{
	
    
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:2.5];
	
	self.view.backgroundColor = [UIColor colorWithRed:(CGFloat)random()/(CGFloat)RAND_MAX green:(CGFloat)random()/(CGFloat)RAND_MAX blue:(CGFloat)random()/(CGFloat)RAND_MAX alpha:1.0];
	
	[UIView commitAnimations];
	
    
    
    //also update the random number...  :)
    int randomNumber = (arc4random() % 99 + 1);
    _numberLabel.text = [NSString stringWithFormat:@"%d",randomNumber];
    
	
}


#pragma mark - Mail


-(IBAction) doInfoButton
{
	NSString *message = [NSString stringWithFormat:@"Would you like to send your questions or comments via email?"];
	//NSString *message = [NSString stringWithFormat:@"Would you like to send us some email and/or write an App Store review?"];
	
	UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:@"Lotto QuickPicker by DK Digital Media" message:message delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
	//UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:@"Am I Alive? by DK Digital Media" message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Send Us Email",@"Write A Review",nil];
	
	[theAlert show];
	//[theAlert dismissWithClickedButtonIndex:x animated:YES];
	
	
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) // Yes
	{
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
		controller.mailComposeDelegate = self;
		
		// Set up recipients
		NSArray *toRecipients = [NSArray arrayWithObject:@"quickerPicker@dkdigital.com"];
		[controller setToRecipients:toRecipients];
		
        NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        NSString *subjectString = [NSString stringWithFormat:@"QuickerPicker %@ User Feedback", appVersion];
        [controller setSubject:subjectString];
        
		
		NSString *diagnosticStr = [NSString stringWithFormat:@"What a great program!!!  :)\n\n\n----------\nUsing an %@ running %@ %@", [[UIDevice currentDevice] model],[[UIDevice currentDevice] systemName],[[UIDevice currentDevice] systemVersion]];
		
		
		[controller setMessageBody:diagnosticStr isHTML:NO];
		
		[self presentModalViewController:controller animated:YES];
		//[controller release];
	}
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
	if (result == MFMailComposeResultSent) {
		NSLog(@"It's away!");
	}
	[self dismissModalViewControllerAnimated:YES];
}



# pragma mark - In-App Purchase Methods
// thanks to
// http://stackoverflow.com/questions/19556336/how-do-you-add-a-in-app-purchase-to-an-iphone-application


- (IBAction)buyProduct:(id)sender
{
    
    if([SKPaymentQueue canMakePayments]){
        whichProduct = [(UIButton *)sender tag];
        
        NSSet *listOfProducts =[NSSet setWithObjects:
                                @"LottoPicker_TipJar1",
                                @"LottoPicker_TipJar2",
                                @"LottoPicker_TipJar3",
                                nil];
        
        
        
        SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:listOfProducts];
        productsRequest.delegate = self;
        [productsRequest start];
        
    }
    else{
        NSLog(@"User cannot make payments due to parental controls");
        //this is called the user cannot make payments, most likely due to parental controls
    }
}


- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    SKProduct *validProduct = nil;
    int count = [response.products count];
    if(count > 0)
    {
        validProduct = [response.products objectAtIndex:whichProduct - 1];
        
        NSLog(@"Title: %@",       validProduct.localizedTitle);
        NSLog(@"Description: %@", validProduct.localizedDescription);
        NSLog(@"Price: %@",       validProduct.price);
        NSLog(@"Id: %@",          validProduct.productIdentifier);
        
        /*
         // Change the prices in the buttons for local currency (first time only)
         bool showLocalPrices = [[NSUserDefaults standardUserDefaults] boolForKey:@"showLocalPrices"];
         if (!showLocalPrices)
         {
         for (int i = 1; i <= 3; i++) {
         UIButton *thisButton = (UIButton *)[self.view viewWithTag:i];
         SKProduct *thisProduct = [response.products objectAtIndex:i-1];
         
         NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
         [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
         [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
         [numberFormatter setLocale:thisProduct.priceLocale];
         NSString *formattedString = [numberFormatter stringFromNumber:thisProduct.price];
         NSLog(@"Formatted Price: %@", formattedString);
         thisButton.titleLabel.text = formattedString;
         
         }
         [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"showLocalPrices"];
         [[NSUserDefaults standardUserDefaults] synchronize];
         
         }
         */
        
        [self purchase:validProduct];
    }
    else if(!validProduct){
        NSLog(@"No products available");
        //this is called if your product id is not valid, this shouldn't be called unless that happens.
    }
}

- (IBAction)purchase:(SKProduct *)product{
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (IBAction) restore
{
    //this is called when the user restores purchases, you should hook this up to a button
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}



- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    NSLog(@"received restored transactions: %i", queue.transactions.count);
    for (SKPaymentTransaction *transaction in queue.transactions)
    {
        if(SKPaymentTransactionStateRestored){
            NSLog(@"Transaction state -> Restored");
            //called when the user successfully restores a purchase
            [self doRemoveAds];
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            break;
        }
        
    }
    
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for(SKPaymentTransaction *transaction in transactions){
        switch (transaction.transactionState){
            case SKPaymentTransactionStatePurchasing: NSLog(@"Transaction state -> Purchasing");
                //called when the user is in the process of purchasing, do not add any of your own code here.
                break;
            case SKPaymentTransactionStatePurchased:
                //this is called when the user has successfully purchased the package (Cha-Ching!)
                [self doRemoveAds]; //you can add your code for what you want to happen when the user buys the purchase here, for this tutorial we use removing ads
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                NSLog(@"Transaction state -> Purchased");
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"Transaction state -> Restored");
                _thanksGraphic.hidden = false;
                
                //add the same code as you did from SKPaymentTransactionStatePurchased here
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                //called when the transaction does not finnish
                if(transaction.error.code != SKErrorPaymentCancelled){
                    NSLog(@"Transaction state -> Cancelled");
                    //the user cancelled the payment ;(
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
        }
    }
}


- (void)doRemoveAds
{
    UIAlertView *thanks = [[UIAlertView alloc] initWithTitle:@"Tip Jar"
                                                     message:@"Thanks for the tip! Philanthropic contributions like yours are greatly appreciated, and what keeps iOS development going strong.  Thanks! 😊"
                                                    delegate:self
                                           cancelButtonTitle:nil
                                           otherButtonTitles:@"You're Welcome",nil];
    
    [thanks show];
    
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"areAdsRemoved"];
    //use NSUserDefaults so that you can load wether or not they bought it
    [[NSUserDefaults standardUserDefaults] synchronize];
    _thanksGraphic.hidden = false;

}



@end
