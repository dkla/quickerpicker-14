//
//  QuickerPickerFlipsideViewController.h
//  QuickerPicker
//
//  Created by Dave Kalin on 4/22/14.
//  Copyright (c) 2014 DK Digital Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <StoreKit/StoreKit.h>


@class QuickerPickerFlipsideViewController;

@protocol QuickerPickerFlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(QuickerPickerFlipsideViewController *)controller;
@end

@interface QuickerPickerFlipsideViewController : UIViewController

@property (weak, nonatomic) id <QuickerPickerFlipsideViewControllerDelegate,MFMailComposeViewControllerDelegate,SKPaymentTransactionObserver, SKProductsRequestDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
- (IBAction)done:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *thanksGraphic;
@property (weak, nonatomic) IBOutlet UIButton *tipButton1;
@property (weak, nonatomic) IBOutlet UIButton *tipButton2;
@property (weak, nonatomic) IBOutlet UIButton *tipButton3;

- (IBAction)buyProduct:(id)sender;
- (IBAction) restore;


@end
