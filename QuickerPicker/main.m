//
//  main.m
//  QuickerPicker
//
//  Created by Dave Kalin on 4/22/14.
//  Copyright (c) 2014 DK Digital Media. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QuickerPickerAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([QuickerPickerAppDelegate class]));
    }
}
