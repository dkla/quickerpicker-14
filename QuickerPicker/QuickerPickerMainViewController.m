//
//  QuickerPickerMainViewController.m
//  QuickerPicker
//
//  Created by Dave Kalin on 4/22/14.
//  Copyright (c) 2014 DK Digital Media. All rights reserved.
//

#import "QuickerPickerMainViewController.h"

@interface QuickerPickerMainViewController ()

@end

@implementation QuickerPickerMainViewController


// stuff for the Accelerometer
UIAccelerationValue	myAccelerometer[3];
#define kAccelerometerFrequency			25 //Hz
#define kFilteringFactor				0.1
#define kMinEraseInterval				1.5
#define kEraseAccelerationThreshold		3.0



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //init Steppers and toggles
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
	
    int Picks = [defaults integerForKey:@"picks"];
	_picksStepper.value = (float)Picks;
	[self updatePicksLabel:nil];
	
	int Range = [defaults integerForKey:@"range"];
	_rangeStepper.value = (float)Range;
	[self updateRangeLabel:nil];
	
	Boolean Bonus = [defaults boolForKey:@"bonus_on"];
	_bonusSwitch.on = Bonus;
	[self doBonusSwitch:nil];
	
	int BonusRange = [defaults integerForKey:@"bonus_range"];
	_bonusRangeStepper.value = (float)BonusRange;
	[self updateBonusLabel:nil];
	
	Boolean Repeats = [defaults boolForKey:@"repeats_on"];
	_repeatSwitch.on = Repeats;
	[self doRepeatSwitch:nil];
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self syncIt];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Flipside View Controller

- (void)flipsideViewControllerDidFinish:(QuickerPickerFlipsideViewController *)controller
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.flipsidePopoverController dismissPopoverAnimated:YES];
    }
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.flipsidePopoverController = nil;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showAlternate"]) {
        [[segue destinationViewController] setDelegate:self];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            UIPopoverController *popoverController = [(UIStoryboardPopoverSegue *)segue popoverController];
            self.flipsidePopoverController = popoverController;
            popoverController.delegate = self;
        }
    }
}

- (IBAction)togglePopover:(id)sender
{
    if (self.flipsidePopoverController) {
        [self.flipsidePopoverController dismissPopoverAnimated:YES];
        self.flipsidePopoverController = nil;
    } else {
        [self performSegueWithIdentifier:@"showAlternate" sender:sender];
    }
}

#pragma mark - My methods


- (IBAction)updatePicksLabel:(id)sender {
    int Picks = (int)_picksStepper.value;
	[_picksLabel setText:[NSString stringWithFormat:@"%1.0d",Picks]];
    [self displayOdds];
    
}

- (IBAction)updateRangeLabel:(id)sender {
    int Range = (int)_rangeStepper.value;
	[_rangeLabel setText:[NSString stringWithFormat:@"%1.0d",Range]];
    [self displayOdds];
}

- (IBAction)updateBonusLabel:(id)sender {
    int bonusRange = (int)_bonusRangeStepper.value;
	[_bonusRangeLabel setText:[NSString stringWithFormat:@"%1.0d",bonusRange]];
    [self displayOdds];
}

- (IBAction)doBonusSwitch:(id)sender {
	
	_bonusRangeLabel.hidden = !_bonusSwitch.on;
	_bonusRangeTitleLabel.enabled = _bonusSwitch.on;

    _bonusRangeStepper.enabled = _bonusSwitch.on;
	_bonusRangeStepper.tintColor = (_bonusSwitch.on ? self.view.tintColor : [UIColor grayColor]);

    [self displayOdds];
    
}

- (IBAction)doRepeatSwitch:(id)sender {
    [self displayOdds];
}

#pragma mark - Odds



double factorial (double value)
{
	if (value > 1) {
		value *= factorial (value - 1);
	}
	return (value);
}

long long ComputeOdds (int Picks, int Range, bool Bonus, int BonusRange, bool Repeats)
{
	long long value;
	
	if (Bonus == TRUE)
	{
		if (Repeats == TRUE)
		{
			value = pow(Range, Picks) * (long long)BonusRange;
		}
		else
		{
			value = (factorial(Range) / (factorial(Picks) * factorial(Range - Picks))) * BonusRange;
		}
	}
	else
	{	//Bonus == FALSE
		if (Repeats == TRUE)
		{
			value = pow(Range, Picks);
		}
		else
		{
			value = factorial(Range) / (factorial(Picks) * factorial(Range - Picks));
		}
	}
	
	
	return (value);
	
}




-(void) displayOdds
{
    int picks = (int)_picksStepper.value;
	int range = (int)_rangeStepper.value;
	int bonusRange = (int)_bonusRangeStepper.value;
    
    
    long long theOdds = ComputeOdds(picks,range, _bonusSwitch.on, bonusRange , _repeatSwitch.on);
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init] ;
	[numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
	
	NSNumber *yy = [NSNumber numberWithLongLong:theOdds];
	[_oddsLabel setText:[NSString stringWithFormat: @"1 in %@",[numberFormatter stringForObjectValue:yy]]];
    
	[_oddsLabel setBackgroundColor:[UIColor colorWithRed:162.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0]];
    
    NSString *OddsText = @"The odds of this Lottery are:";
	[_oddsLabelHeader setText: OddsText];
    
    
    
    
}

-(void) syncIt
{
    int picks = (int)_picksStepper.value;
	int range = (int)_rangeStepper.value;
	int bonusRange = (int)_bonusRangeStepper.value;

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setInteger:picks forKey:@"picks"];
	[defaults setInteger:range forKey:@"range"];
	[defaults setBool:_bonusSwitch.on forKey:@"bonus_on"];
	[defaults setInteger:bonusRange forKey:@"bonus_range"];
	[defaults setBool:_repeatSwitch.on forKey:@"repeats_on"];
    [defaults synchronize];

}


#pragma mark - Lotto



- (IBAction)pickNumbers:(id)sender
{

    [self syncIt];
    
  	Boolean repeatsAllowed = _repeatSwitch.on;
    int picks = (int)_picksStepper.value;
	int range = (int)_rangeStepper.value;
    NSString *thisLottoString;
	
    if (!repeatsAllowed)
    {
        NSMutableSet * thisLottoNumbersSet = [NSMutableSet setWithCapacity:picks];
        while ([thisLottoNumbersSet count] < picks ) {
            NSNumber * randomNumber = [NSNumber numberWithInt:(arc4random() % range + 1)];
            [thisLottoNumbersSet addObject:randomNumber];
        }
        
        NSArray *unsortedArray2 = [thisLottoNumbersSet allObjects];
        NSArray *thisLottosNumbers = [unsortedArray2 sortedArrayUsingSelector:@selector(compare:)];
        
        
        thisLottoString = [NSString stringWithFormat:@"%@",  [thisLottosNumbers componentsJoinedByString:@" - "]];
        
    }
 	else  // do a lotto with repeating numbers
    {
        NSMutableArray *thisLotto = [[NSMutableArray alloc] init];
        int n;
        for (n=1; n<=picks; n++)
        {
            NSNumber * randomNumber = [NSNumber numberWithInt:(arc4random() % range + 1)];
            [thisLotto addObject:randomNumber];

        }
        
        NSArray *thisLottosNumbers = [thisLotto sortedArrayUsingSelector:@selector(compare:)];
 
        thisLottoString = [NSString stringWithFormat:@"%@",  [thisLottosNumbers componentsJoinedByString:@" - "]];

    }
    
    
        // do Bonus Ball (if available)
        if (_bonusSwitch.on == TRUE)
        {
            int bonusRange = (int)_bonusRangeStepper.value;
            int x = (arc4random() % bonusRange) +1;
            
            NSString *str = [NSString stringWithFormat:@"\nBonus: %d", x];
            thisLottoString = [thisLottoString stringByAppendingString:str];
        }
        
        [_oddsLabel setBackgroundColor:[UIColor colorWithRed:204.0/255.0 green:255.0/255.0 blue:102.0/255.0 alpha:0.85]];
        
        NSString *OddsText = @"Your Lottery Numbers are:";
        [_oddsLabelHeader setText: OddsText];
        
        
        [UIView beginAnimations:nil	context:NULL];
        [UIView setAnimationDuration:0.8888];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:_oddsLabel cache:YES];
        [UIView commitAnimations];
        [_oddsLabel setText: thisLottoString];
        
    }
    
    
    
    @end
