//
//  QuickerPickerMainViewController.h
//  QuickerPicker
//
//  Created by Dave Kalin on 4/22/14.
//  Copyright (c) 2014 DK Digital Media. All rights reserved.
//

#import "QuickerPickerFlipsideViewController.h"

@interface QuickerPickerMainViewController : UIViewController <QuickerPickerFlipsideViewControllerDelegate, UIPopoverControllerDelegate>

@property (strong, nonatomic) UIPopoverController *flipsidePopoverController;
- (IBAction)pickNumbers:(id)sender;
- (IBAction)updatePicksLabel:(id)sender;
- (IBAction)updateRangeLabel:(id)sender;
- (IBAction)updateBonusLabel:(id)sender;

- (IBAction)doBonusSwitch:(id)sender;
- (IBAction)doRepeatSwitch:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *GoButton;
@property (weak, nonatomic) IBOutlet UIStepper *picksStepper;
@property (weak, nonatomic) IBOutlet UIStepper *rangeStepper;
@property (weak, nonatomic) IBOutlet UIStepper *bonusRangeStepper;

@property (weak, nonatomic) IBOutlet UILabel *picksLabel;
@property (weak, nonatomic) IBOutlet UILabel *rangeLabel;
@property (weak, nonatomic) IBOutlet UILabel *bonusRangeLabel;
@property (weak, nonatomic) IBOutlet UILabel *bonusRangeTitleLabel;

@property (weak, nonatomic) IBOutlet UISwitch *bonusSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *repeatSwitch;

@property (weak, nonatomic) IBOutlet UILabel *oddsLabel;
@property (weak, nonatomic) IBOutlet UILabel *oddsLabelHeader;





@end
